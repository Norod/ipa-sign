Improved ipa_sign (froked from ota-tools)
======================================

A bash script for resigning IPA files 

* Resign the ipa with a different certificate
* Replace the provisioning profile 
* Replace the entitlements xcent data
* Inspect existing IPA packages
* List available certificates

##Usage:

    Usage: ipa_sign Application.ipa foo/bar.mobileprovision "iPhone Distribution: I can haz code signed" 
    Usage: ipa_sign Application.ipa foo/bar.mobileprovision "iPhone Distribution: I can haz code signed" foo/Entitlements.xcent
    Usage: ipa_sign -i Application.ipa

    Options:
      
      -i    Only inspect the package. Do not resign it.
      -l    List certificates and exit

This bash script has been imported from my [GitHub Repository](https://github.com/Norod/ota-tools) which was previously forked from [this project](https://github.com/RichardBronosky/ota-tools)